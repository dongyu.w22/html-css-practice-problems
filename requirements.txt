asgiref==3.7.2
Django==5.0.3
django-debug-toolbar==4.3.0
Pygments==2.17.2
PyYAML==6.0.1
sqlparse==0.4.4
